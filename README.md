## Proyecto desarrollado como prueba tecnica para Guros

**Lenguaje**: Javascript, NodeJs

**Base de datos**: Firebase Firestore

**Hosting:** AWS EC2

**URL API:** http://mutations.eberreta.tk

**End points API:** 

  POST `http://mutations.eberreta.tk/mutations`

  GET `http://mutations.eberreta.tk/stats`


### Pasos para ejecutar local: 

`npm install`

`npm run dev`
