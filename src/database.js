const firebase =  require('firebase')

var firebaseConfig = {
    apiKey: "AIzaSyDIuB3qb7Tf8wR30Xv4knzKuY4xyoZs-I4",
    authDomain: "mutation-analyzer.firebaseapp.com",
    projectId: "mutation-analyzer",
    storageBucket: "mutation-analyzer.appspot.com",
    messagingSenderId: "39405944023",
    appId: "1:39405944023:web:c1f3c55b824057d4544492"
};

firebase.initializeApp(firebaseConfig);
const firestore = firebase.firestore();

// DATABASE METHODS 

const searchResult = async (dna) =>{
    const dnaID = dna.join('');
    const result = await firestore.collection('mutations').doc(dnaID).get();
    return result.exists ? { ...result.data()} : undefined;

}
const storeResult = async (dna, result) =>{
    const dnaID = dna.join('');
    await firestore.collection('mutations').doc(dnaID).set({result});
}

const getResults = async () =>{
    let mutations = []
    const mutationsDB = await firestore.collection('mutations').get();
    mutationsDB.forEach(doc => {
        console.log(doc.id)
        mutations.push({...doc.data(), id:doc.id});
    });
    return mutations;
}

const reset = async () =>{
    

}

module.exports = {searchResult,storeResult, getResults};