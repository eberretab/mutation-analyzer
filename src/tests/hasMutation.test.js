const hasMutation = require('../hasMutation')

test('Has Mutation',()=>{   
    const dna = ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
    expect(hasMutation(dna)).toBe(true)
});

test('No Mutation',()=>{   
    const dna = ["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"]
    expect(hasMutation(dna)).toBe(false)
});