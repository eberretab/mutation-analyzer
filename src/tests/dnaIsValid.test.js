const dnaIsValid = require('../dnaIsValid')

test('ADN valid',()=>{   
    const dna = ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
    expect(dnaIsValid(dna)).toBe(true)
});

test('ADN not valid',()=>{   
    const dna = ["ARGCGA","CADTGF","TTATTT","AGACGG","GCGTCA","TCACTG"]
    expect(dnaIsValid(dna)).toBe(false)
});


test('ADN not valid nxn',()=>{   
    const dna = ["ARGCA","CADTGF","TTATTT","AGACGG","GCGTCA","TCACTG"]
    expect(dnaIsValid(dna)).toBe(false)
});