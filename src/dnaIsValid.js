const  dnaIsValid = (dna) => {
    // A T C G  Only 
    const anoterLetters =  dna.join('').split('').filter(letter => !["A","T","C","G"].includes(letter));
    
    // Matrix nxn
    let nxn = true;
    for (let index = 0; index < dna.length; index++) {
        if(dna[index].length != dna.length){
            nxn = false;
            index = dna.length
        }
    }

    return anoterLetters.length == 0 && nxn; 
}

module.exports = dnaIsValid;