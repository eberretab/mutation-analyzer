const  hasMutation = (dna) => {
    let currentLetter = {letter : "",count: 1}
    const rowCount = dna.length;
    const colCount = dna[0].length;
    let mutations = 0;
    
    // Horizontal
    for (let i = 0; i < rowCount; i++) {
        let base = dna[i].split('');
        for (let j = 0; j < colCount; j++) {
            let letter = base[j];
            currentLetter.letter = letter;
            if(j+1 < colCount && letter == base[j+1]){
                currentLetter.count++;
            }else{
                currentLetter.count=1;
            }
            if(currentLetter.count==4){
                mutations++;
            }
        }
    }
    // Vertical
    for (let i = 0; i < rowCount; i++) {
        for (let j = 0; j < colCount; j++) {
            let letter = dna[j][i];
            currentLetter.letter = letter;
            if(j+1 < colCount && letter == dna[j+1][i]){
                currentLetter.count++;
            }else{
                currentLetter.count=1;
            }
            if(currentLetter.count==4){
                mutations++;
            }
        }
    }
    // oblicuo  
    for (let i = 0; i < rowCount; i++) {
        for (let j = 0; j <= i; j++) {
            let letter = (dna[i-j][j]);
            let next = dna[i-j-1]? dna[i-j-1][j+1]:'';
            currentLetter.letter = letter;
            if(i-j-1< j &&  i+1 < i && letter == dna[i-j-1][j+1]){
                next = dna[i-j+1][j+1];
                currentLetter.count++;
            }else{
                currentLetter.count=1;
            }
            if(currentLetter.count==4){
                mutations++;
            }
        }
    }
    for (let i = 0; i < rowCount; i++) {
        for (let j = 0; j < colCount-i-1; j++) { 
            let letter = dna[dna.length-j-1][j+i+1];
            let next = dna[dna.length-j-2] ? dna[dna.length-j-2][j+i+2] : ''
            currentLetter.letter = letter;
            if(dna.length-j-2 < colCount && j+i+2 < colCount && letter==dna[dna.length-j-2][j+i+2]){
                currentLetter.count++;
            }else{
                currentLetter.count=1;
            }
            if(currentLetter.count==4){
                mutations++;
            }
        }
    }
    // oblicuo  inverso
    for (let i = 0; i < rowCount; i++) {
        for (let j = 0; j <= i; j++) {
            let letter = dna[i-j][rowCount-j-1];
            let next = dna[i-j-1] ?  dna[i-j-1][rowCount-j-2] : ''
            currentLetter.letter = letter;
            if(i-j-1>=0 && rowCount-j-2 < rowCount && letter==dna[i-j-1][rowCount-j-2]){
                currentLetter.count++;
            }else{
                currentLetter.count=1;
            }
            if(currentLetter.count==4){
                mutations++;
            }
        }
    }
    for (let i = 0; i < rowCount; i++) {
        for (let j = 0; j < colCount-i-1; j++) {
            let letter = dna[dna.length-j-1][rowCount-j-2-i];
            let next = dna[dna.length-j-1-1][rowCount-j-2-i-1];
            currentLetter.letter = letter;
            if(letter == next){
                currentLetter.count++;
            }else{
                currentLetter.count= 1;
            }
            if(currentLetter.count==4){
                mutations++
            }
        }
    }
    return (mutations>1);
}


module.exports = hasMutation;