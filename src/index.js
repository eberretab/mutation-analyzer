const express = require('express');
const app = express();
app.use(express.json());

app.use('/',require('./router'));

app.listen('3000', () =>{
    console.log("listen on port 3000")
})