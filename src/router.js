const { Router} = require('express');
const router = Router();
const database = require('./database');
const hasMutation = require('./hasMutation')
const dnaIsValid = require('./dnaIsValid')

router.post('/mutation',async (req, response)=>{
    const { dna } = req.body;
    if(dna){
        if(dnaIsValid(dna)){
            let resultDB = await database.searchResult(dna);
            let result;
            if(resultDB){
                result = resultDB.result
                console.log("Consultado en cache")
            }else{
                result = hasMutation(dna);
                await database.storeResult(dna,result)
                console.log("Anailsis realizado")
            }
            if(result){
                response.status(200).send("TIENE MUTACIÓN");
            }else{
                response.status(403).send("NO TIENE MUTACIÓN");
            }
        }else{
            response.status(400).send("Bad DNA")
        }
    }else{
        response.status(400).send("Matrix not found")
    }
})

router.get('/stats', async (req,res)=>{
    const dnas = await database.getResults();
    let mutations = 0;
    let noMutations = 0;
    dnas.forEach(adn => {
        if(adn.result){
            mutations++;
        }else{
            noMutations++;
        }
    });

    const ratio = noMutations==0?  0 : (mutations/noMutations);
    res.status(200).json({
        "count_mutations":mutations,
        "count_no_mutations":noMutations,
        "ratio": ratio
    });
})


module.exports = router;